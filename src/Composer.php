<?php

namespace WP_Instances\Worker;

class Composer
{
    private string $root_path;
    private array $json_paths = array();
    private array $composer_plugins = array();
    private array $composer_themes = array();

    public function __construct()
    {
        $this->fetch_and_set_composer_root_path();
        $this->fetch_and_set_composer_json_paths();

        if (isset($this->json_paths['lock'])) {
            $this->composer_lock = $this->read_composer_lock();
            $this->composer_plugins = $this->fetch_composer_plugins();
            $this->composer_themes = $this->fetch_composer_themes();
        }
        // $this->composer_json = $this->get_composer_json($this->composer_json_paths['setup']);
    }

    private function fetch_and_set_composer_root_path()
    {
        $server_root_path = $_SERVER['DOCUMENT_ROOT'];

        $this->root_path = $server_root_path;
    }

    private function fetch_and_set_composer_json_paths()
    {
        $composer_paths = array();

        $json_file = $this->root_path . DIRECTORY_SEPARATOR . 'composer.json';
        $lock_file = $this->root_path . DIRECTORY_SEPARATOR . 'composer.lock';

        if (!file_exists($lock_file)) {
            $this->json_paths = array(
                'json' => null,
                'lock' => null
            );
        }

        if (file_exists($json_file)) {
            $composer_paths['json'] = $json_file;
        }

        if (file_exists($lock_file)) {
            $composer_paths['lock'] = $lock_file;
        }

        $this->json_paths = $composer_paths;
    }

    public function read_composer_json(): string
    {
        $file = $this->json_paths['json'];

		if(!$file) {
			return '';
		}

        $file_content = file_get_contents($file);

        return $file_content;
    }

    public function read_composer_lock(): string
    {
        $file = $this->json_paths['lock'];

		if(!$file) {
			return '';
		}

        $file_content = file_get_contents($file);

        return $file_content;
    }

    public function fetch_composer_plugins()
    {
        $composer_json = $this->read_composer_lock();
        $composer_json = json_decode($composer_json, true);

        $plugins = array_filter($composer_json['packages'], function ($package) {
            return $package['type'] === 'wordpress-plugin';
        });

        return $plugins;
    }

    public function fetch_composer_themes()
    {
        $composer_json = $this->read_composer_lock();
        $composer_json = json_decode($composer_json, true);

        $themes = array_filter($composer_json['packages'], function ($package) {
            return $package['type'] === 'wordpress-theme';
        });

        return $themes;
    }

    public function is_plugin_composer_installed(string $plugin_path): bool
    {
        $wordpress_plugin_name = explode('/', $plugin_path)[0];

        $composer_plugin_names = array_map(function ($plugin) {
            $plugin_name = explode('/', $plugin['name'])[1];
            return $plugin_name;
        }, $this->composer_plugins);

        $composer_plugin_names = array_flip($composer_plugin_names);

        return isset($composer_plugin_names[$wordpress_plugin_name]);
    }

    public function is_theme_composer_installed(string $theme_path): bool
    {
        $wordpress_theme_name = explode('/', $theme_path)[0];

        $composer_theme_names = array_map(function ($theme) {
            $theme_name = explode('/', $theme['name'])[1];
            return $theme_name;
        }, $this->composer_themes);

        $composer_theme_names = array_flip($composer_theme_names);

        return isset($composer_theme_names[$wordpress_theme_name]);
    }

	public function has_composer_json(string $plugin_path): bool
	{
		$exploded_plugin_path = explode('/', $plugin_path);

		if(count($exploded_plugin_path) === 1) {
			return false;
		}

		$plugin_directory_name = $exploded_plugin_path[0];
		$plugin_path = sprintf('%s/%s/composer.json', PLUGINDIR, $plugin_directory_name);

		return file_exists($plugin_path);
	}
}
