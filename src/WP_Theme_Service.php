<?php

namespace WP_Instances\Worker;

class WP_Theme_Service
{
    public function get_all_themes()
    {
        $themes = wp_get_themes();
		return $themes;

        // var_dump($themes);

        foreach ($themes as $theme_name => $theme_data) {
            $themes[$theme_name] = $this->get_theme_details($theme_name, $theme_data);
        }

        return $themes;
    }

    public static function get_active_theme()
    {
        $active_theme = wp_get_theme();

        return $active_theme;
    }

    public function get_theme(string $theme_name)
    {
        $themes = $this->get_all_themes();

        $theme = $themes[$theme_name] = $this->get_theme_details($theme_name, $themes[$theme_name]);

        return $theme;
    }

    public function get_theme_details(string $theme_name, $theme_data): array
    {
        $theme_DTO = new Theme_DTO(
            name: $theme_data->get('Name'),
            author: $theme_data->get('Author'),
            status: $theme_data->get('Status'),
            version: $theme_data->get('Version'),
            stylesheet: $theme_data->get_stylesheet(),
            block_theme: $theme_data->is_block_theme(),
            text_domain: $theme_data->get('TextDomain'),
            template: $theme_data->get_template(),
            uri: $theme_data->get('Uri'),
            description: $theme_data->get('Description'),
            author_uri: $theme_data->get('AuthorURI'),
            tags: $theme_data->get('Tags'),
            domain_path: $theme_data->get('DomainPath'),
            requires_wp: $theme_data->get('RequiresWP'),
            requires_php: $theme_data->get('RequiresPHP'),
            update_uri: $theme_data->get('UpdateURI'),
            active: false,
            network_enabled: false,
            composer_installed: false
        );

        $theme = new Theme($theme_DTO);

        return $theme->to_array();
    }
}
