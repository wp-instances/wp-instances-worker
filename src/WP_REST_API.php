<?php

namespace WP_Instances\Worker;

use WP_Instances\Worker\WP_Plugin_Service;
use WP_Instances\Worker\WP_Theme_Service;

class WP_REST_API
{
    public function __construct()
    {
        add_action('rest_api_init', array($this, 'register_routes'));
    }

    public function register_routes()
    {
        register_rest_route(
            'wp-instances-worker/v1',
            '/overview',
            array(
                'methods' => 'GET',
                'callback' => array($this, 'get_details'),
                'permission_callback' => '__return_true',
            )
        );
    }

    public function get_details(): \WP_REST_Response
    {
        $core_settings = WP_Core_Settings::get();

        $data = array(
            'core' => $core_settings,
            'plugins' => (new WP_Plugin_Service())->get_all_plugins(),
            'themes' => (new WP_Theme_Service())->get_all_themes(),
        );

        return new \WP_REST_Response($data, 200);
    }

    private function get_php_version()
    {
        return phpversion();
    }

    private function get_mysql_version()
    {
        global $wpdb;
        return $wpdb->db_version();
    }

    private function get_plugins(): array
    {
        if (!function_exists('get_plugins')) {
            require_once ABSPATH . 'wp-admin/includes/plugin.php';
        }

        $plugins = get_plugins();

        foreach ($plugins as $key => $plugin) {
            $plugins[$key]['IsActive'] = is_plugin_active($key);
        }

        return $plugins;
    }

    private function get_themes(): array
    {
        $themes = wp_get_themes();
        // var_dump($themes);
        // $active_theme = wp_get_theme();
        // var_dump($active_theme);

        // foreach ($themes as $key => $theme) {
        //     $themes[$key]['IsActive'] = $theme->get('Name') === $active_theme;
        // }

        var_dump($themes);

        return $themes;
    }

    private function get_site_health()
    {
        if (!class_exists('WP_Site_Health')) {
            require ABSPATH . 'wp-admin/includes/class-wp-site-health.php';
        }

        $site_health = new \WP_Site_Health();
        $tests = $site_health->get_tests();

        return $tests;

        $results = array(
            'direct' => array(),
            'async' => array(),
        );

        // Run direct tests
        foreach ($tests['direct'] as $test) {
            if (is_callable($test['test'])) {
                $result = call_user_func($test['test']);
                $results['direct'][] = $result;
            }
        }

        // Run async tests
        foreach ($tests['async'] as $test) {
            if (is_callable($test['test'])) {
                $result = call_user_func($test['test']);
                $results['async'][] = $result;
            }
        }

        return $results;
    }

    // public function permissions_check()
    // {
    //     return current_user_can('manage_options');
    // }
}
