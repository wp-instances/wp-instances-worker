<?php

namespace WP_Instances\Worker;
use WP_Instances\Worker\Composer;

class WP_Plugin
{
    private string $path;
    private string $name;
    private ?string $uri;
    private string $version;
    private ?string $description;
    private string $author;
    private ?string $author_uri;
    private string $text_domain;
    private ?string $domain_path;
    private string $network;
    private ?string $requires_wp;
    private ?string $requires_php;
    private ?string $update_uri;
    private ?string $title;
    private string $author_name;
    private bool $is_active;
    private bool $network_active;
    private bool $is_must_use;
	private bool $has_composer_json;
    private bool $is_composer_installed;
	private Composer $composer;

    public function __construct(
        string $path,
        string $name,
        string $version,
        string $author,
        string $network,
        bool $network_active,
        string $title,
        string $author_name,
        bool $is_must_use,
		Composer $composer,
        string $uri = '',
        string $description = '',
        string $author_uri = '',
        string $text_domain = '',
        string $domain_path = '',
        string $requires_wp = '',
        string $requires_php = '',
        string $update_uri = '',
    ) {
        $this->path = $path;
        $this->name = $name;
        $this->uri = $uri;
        $this->version = $version;
        $this->description = $description;
        $this->author = $author;
        $this->author_uri = $author_uri;
        $this->text_domain = $text_domain;
        $this->domain_path = $domain_path;
        $this->network = $network;
        $this->network_active = $network_active;
        $this->requires_wp = $requires_wp;
        $this->requires_php = $requires_php;
        $this->update_uri = $update_uri;
        $this->title = $title;
        $this->author_name = $author_name;
        $this->is_active = $this->fetch_is_active();
        $this->is_must_use = $is_must_use;
		$this->composer = $composer;
		$this->has_composer_json = $this->check_has_composer_json();
        $this->is_composer_installed = $this->check_is_composer_installed();
    }

    public function get_path(): string
    {
        return $this->path;
    }

    public function get_name(): string
    {
        return $this->name;
    }

    public function get_uri(): string
    {
        return $this->uri;
    }

    public function get_version(): string
    {
        return $this->version;
    }

    public function get_description(): string
    {
        return $this->description;
    }

    public function get_author(): string
    {
        return $this->author;
    }

    public function get_author_uri(): string
    {
        return $this->author_uri;
    }

    public function get_text_domain(): string
    {
        return $this->text_domain;
    }

    public function get_domain_path(): string
    {
        return $this->domain_path;
    }

    public function get_network(): string
    {
        return $this->network;
    }

    public function is_network_active(): bool
    {
        return $this->network_active;
    }

    public function get_requires_wp(): string
    {
        return $this->requires_wp;
    }

    public function get_requires_php(): string
    {
        return $this->requires_php;
    }

    public function get_update_uri(): string
    {
        return $this->update_uri;
    }

    public function get_title(): string
    {
        return $this->title;
    }

    public function get_author_name(): string
    {
        return $this->author_name;
    }

	public function fetch_is_active() {
		return is_plugin_active($this->path);
	}

    public function is_active(): bool
    {
        return $this->is_active;
    }

    public function is_must_use(): bool
    {
        return $this->is_must_use;
    }

	public function check_has_composer_json() {
		return $this->composer->has_composer_json($this->get_path());
	}

    public function is_composer_installed(): bool
    {
        return $this->is_composer_installed;
    }

	public function check_is_composer_installed() {
		$this->composer = new Composer();
		return $this->composer->is_plugin_composer_installed($this->path);
	}

    public function to_array(): array
    {
        return [
            'path' => $this->get_path(),
            'name' => $this->get_name(),
            'uri' => $this->get_uri(),
            'version' => $this->get_version(),
            'description' => $this->get_description(),
            'author' => $this->get_author(),
            'author_uri' => $this->get_author_uri(),
            'text_domain' => $this->get_text_domain(),
            'domain_path' => $this->get_domain_path(),
            'network' => $this->get_network(),
            'requires_wp' => $this->get_requires_wp(),
            'requires_php' => $this->get_requires_php(),
            'update_uri' => $this->get_update_uri(),
            'title' => $this->get_title(),
            'author_name' => $this->get_author_name(),
            'active' => $this->is_active(),
            'network_active' => $this->is_network_active(),
            'must_use' => $this->is_must_use(),
            'composer_installed' => $this->is_composer_installed(),
        ];
    }
}
