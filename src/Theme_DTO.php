<?php

namespace WP_Instances\Worker;

class Theme_DTO
{
    private string $name;
    private string $uri;
    private string $description;
    private string $author;
    private string $author_uri;
    private string $version;
    private string $template;
    private string $stylesheet;
    private bool $block_theme;
    private string $status;
    private array $tags;
    private string $text_domain;
    private string $domain_path;
    private string $requires_wp;
    private string $requires_php;
    private string $update_uri;
    private bool $active;
    private bool $network_enabled;
    private bool $composer_installed;
    
    public function __construct(
        string $name,
        string $author,
        bool $block_theme,
        bool $network_enabled,
        bool $composer_installed,
        string $status = '',
        string $template = '',
        string $stylesheet = '',
        string $version = '',
        string $text_domain = '',
        string $uri = '',
        string $description = '',
        string $author_uri = '',
        array $tags = array(),
        string $domain_path = '',
        string $requires_wp = '',
        string $requires_php = '',
        string $update_uri = '',
        bool $active = false,
    ) {
        $this->name = $name;
        $this->uri = $uri;
        $this->description = $description;
        $this->author = $author;
        $this->author_uri = $author_uri;
        $this->version = $version;
        $this->template = $template;
        $this->stylesheet = $stylesheet;
        $this->block_theme = $block_theme;
        $this->status = $status;
        $this->tags = $tags;
        $this->text_domain = $text_domain;
        $this->domain_path = $domain_path;
        $this->requires_wp = $requires_wp;
        $this->requires_php = $requires_php;
        $this->update_uri = $update_uri;
        $this->active = $active;
        $this->network_enabled = $network_enabled;
        $this->composer_installed = $composer_installed;
    }
    
    public function get_name(): string
    {
        return $this->name;
    }
    
    public function get_uri(): string
    {
        return $this->uri;
    }
    
    public function get_version(): string
    {
        return $this->version;
    }
    
    public function get_description(): string
    {
        return $this->description;
    }
    
    public function get_author(): string
    {
        return $this->author;
    }
    
    public function get_author_uri(): string
    {
        return $this->author_uri;
    }
    
    public function get_text_domain(): string
    {
        return $this->text_domain;
    }
    
    public function is_block_theme(): bool
    {
        return $this->block_theme;
    }
    
    public function get_stylesheet(): string
    {
        return $this->stylesheet;
    }
    
    public function get_domain_path(): string
    {
        return $this->domain_path;
    }
    
    public function get_requires_wp(): string
    {
        return $this->requires_wp;
    }
    
    public function get_requires_php(): string
    {
        return $this->requires_php;
    }
    
    public function get_update_uri(): string
    {
        return $this->update_uri;
    }
    
    public function get_template(): string
    {
        return $this->template;
    }
    
    public function get_status(): string
    {
        return $this->status;
    }
    
    public function get_tags(): array
    {
        return $this->tags;
    }
    
    public function is_active(): ?bool
    {
        return $this->active;
    }
    
    public function is_network_enabled(): ?bool
    {
        return $this->network_enabled;
    }
    
    public function is_composer_installed(): ?bool
    {
        return $this->composer_installed;
    }
    
    public function to_array(): array {
        return array(
            'name' => $this->get_name(),
            'uri' => $this->get_uri(),
            'version' => $this->get_version(),
            'description' => $this->get_description(),
            'author' => $this->get_author(),
            'author_uri' => $this->get_author_uri(),
            'text_domain' => $this->get_text_domain(),
            'block_theme' => $this->is_block_theme(),
            'stylesheet' => $this->get_stylesheet(),
            'domain_path' => $this->get_domain_path(),
            'requires_wp' => $this->get_requires_wp(),
            'requires_php' => $this->get_requires_php(),
            'update_uri' => $this->get_update_uri(),
            'template' => $this->get_template(),
            'status' => $this->get_status(),
            'tags' => $this->get_tags(),
            'active' => $this->is_active(),
            'network_enabled' => $this->is_network_enabled(),
            'composer_installed' => $this->is_composer_installed()
        );
    }
}