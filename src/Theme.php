<?php

namespace WP_Instances\Worker;

class Theme
{
    private Theme_DTO $theme_DTO;
    private bool $is_active;
    private bool $is_network_enabled;
    private bool $is_composer_installed;

    public function __construct(
        Theme_DTO $theme_DTO
    ) {
        $this->theme_DTO = $theme_DTO;
        $this->is_active = $this->check_is_active();
        $this->is_network_enabled = $this->check_is_network_enabled();
        $this->is_composer_installed = $this->check_is_composer_installed();
    }

    private function check_is_active(): bool
    {
        $active_theme = WP_Theme_Service::get_active_theme();

        return $active_theme->get('Name') === $this->theme_DTO->get_name();
    }

    private function check_is_network_enabled(): bool
    {
        $theme = wp_get_theme($this->theme_DTO->get_stylesheet());
        return isset($theme->allowed['network']) && $theme->allowed['network'];
    }

    private function check_is_composer_installed(): bool
    {
        $composer = new Composer();
        return $composer->is_theme_composer_installed($this->theme_DTO->get_stylesheet());
    }

    public function to_array(): array
    {
        $theme = $this->theme_DTO->to_array();
        $theme['active'] = $this->is_active;
        $theme['network_enabled'] = $this->is_network_enabled;
        $theme['composer_installed'] = $this->is_composer_installed;

        return $theme;
    }
}
