<?php

namespace WP_Instances\Worker;

use WP_Instances\Worker\WP_Plugin;
use WP_Instances\Worker\Plugin;

class WP_Plugin_Service
{
    public function __construct()
    {
        if (!function_exists('get_plugins')) {
            require_once ABSPATH . 'wp-admin/includes/plugin.php';
        }
    }

    public function get_all_plugins(): array
    {
        $plugins = get_plugins();
        $must_use_plugins = get_mu_plugins();

        $plugins = array_merge($plugins, $must_use_plugins);

        $plugins_data = array();

        foreach ($plugins as $plugin_path => $plugin_data) {
            $plugin = new WP_Plugin(
				path: $plugin_path,
				name: $plugin_data['Name'],
				version: $plugin_data['Version'],
				uri: $plugin_data['PluginURI'],
				description: $plugin_data['Description'],
				author: $plugin_data['Author'],
				author_uri: $plugin_data['AuthorURI'],
				text_domain: $plugin_data['TextDomain'],
				domain_path: $plugin_data['DomainPath'],
				network: $plugin_data['Network'],
				requires_wp: $plugin_data['RequiresWP'],
				requires_php: $plugin_data['RequiresPHP'],
				update_uri: $plugin_data['UpdateURI'],
				title: $plugin_data['Title'],
				author_name: $plugin_data['AuthorName'],
				network_active: false,
				is_must_use: false,
				composer: new Composer(),
			);

            $plugins_data[] = $plugin;
        }

        return $plugins_data;
    }

    public function get_active_plugins()
    {
        $active_plugins = get_option('active_plugins');

        return $active_plugins;
    }

    public function get_must_use_plugins()
    {
        $must_use_plugins = get_mu_plugins();

        return $must_use_plugins;
    }
}
