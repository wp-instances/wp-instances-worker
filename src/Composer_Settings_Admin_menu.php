<?php

namespace WP_Instances\Worker;

class Composer_Settings_Admin_Menu
{
    public function __construct()
    {
        add_action('admin_menu', [$this, 'register_menu']);
    }

    public function register_menu()
    {
        add_submenu_page(
            'options-general.php',
            'WP Instances Worker',
            'WP Instances Worker',
            'manage_options',
            'wp-instances-worker-settings',
            [$this, 'display_settings_page']
        );
    }

    public function display_settings_page()
    {
        ob_start();
        ?>
        Composer Settings ...
        <?php
        $output = ob_get_contents();
        ob_end_clean();
        
        echo $output;
    }

}