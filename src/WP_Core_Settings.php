<?php

namespace WP_Instances\Worker;

class WP_Core_Settings
{
	private string $version;
	private string $site_language;
	private string $timezone;
	private string $permalink_structure;
	private bool $is_https;
	private bool $is_multisite;
	private bool $users_can_register;
	private bool $is_blog_public;
	private string $default_comment_status;
	private string $environment_type;
	private string $home_url;
	private string $site_url;

	private function __construct()
	{
	}

	private function fetch_version(): string
	{
		return get_bloginfo('version');
	}

	public function get_version(): string
	{
		return $this->version;
	}

	private function fetch_site_language(): string
	{
		return get_bloginfo('language');
	}

	public function get_site_language(): string
	{
		return $this->site_language;
	}

	private function fetch_timezone(): string
	{
		return get_option('timezone_string');
	}

	public function get_timezone(): string
	{
		return $this->timezone;
	}

	private function fetch_permalink_structure(): string
	{
		return get_option('permalink_structure');
	}

	public function get_permalink_structure(): string
	{
		return $this->permalink_structure;
	}

	private function fetch_is_https(): bool
	{
		return is_ssl() ? true : false;
	}

	public function is_https(): bool
	{
		return $this->is_https;
	}

	private function fetch_is_multisite(): bool
	{
		return is_multisite() ? true : false;
	}

	public function is_multisite(): bool
	{
		return $this->is_multisite;
	}

	private function fetch_users_can_register(): bool
	{
		return get_option('users_can_register') ? true : false;
	}

	public function get_users_can_register(): bool
	{
		return $this->users_can_register;
	}

	private function fetch_is_blog_public(): bool
	{
		return get_option('blog_public') ? true : false;
	}

	public function is_blog_public(): bool
	{
		return $this->is_blog_public;
	}

	private function fetch_default_comment_status(): string
	{
		return get_option('default_comment_status');
	}

	public function get_default_comment_status(): string
	{
		return $this->default_comment_status;
	}

	private function fetch_environment_type(): string
	{
		return defined('WP_ENV') ? WP_ENV : 'production';
	}

	public function get_environment_type(): string
	{
		return $this->environment_type;
	}

	private function fetch_home_url(): string
	{
		return home_url();
	}

	public function get_home_url(): string
	{
		return $this->home_url;
	}

	private function fetch_site_url(): string
	{
		return site_url();
	}

	public function get_site_url(): string
	{
		return $this->site_url;
	}

	public static function get(): WP_Core_Settings
	{
		$core_settings = new self();
		$core_settings->version = $core_settings->fetch_version();
		$core_settings->site_language = $core_settings->fetch_site_language();
		$core_settings->timezone = $core_settings->fetch_timezone();
		$core_settings->permalink_structure = $core_settings->fetch_permalink_structure();
		$core_settings->is_https = $core_settings->fetch_is_https();
		$core_settings->is_multisite = $core_settings->fetch_is_multisite();
		$core_settings->users_can_register = $core_settings->fetch_users_can_register();
		$core_settings->is_blog_public = $core_settings->fetch_is_blog_public();
		$core_settings->default_comment_status = $core_settings->fetch_default_comment_status();
		$core_settings->environment_type = $core_settings->fetch_environment_type();
		$core_settings->home_url = $core_settings->fetch_home_url();
		$core_settings->site_url = $core_settings->fetch_site_url();

		return $core_settings;
	}
}
