<?php
/**
 * Plugin Name:     WP Instances Worker
 * Description:     WP Instances Worker
 * Author:          Filip Van Reeth
 * Author URI:      filipvanreeth.com
 * Text Domain:     wp-instances-worker
 * Domain Path:     /languages
 * Version:         0.1.0
 * Network:         true
 * Requires PHP:    8.0
 *
 * @package         WP_Instances_Worker
 */

use WP_Instances\Worker\Composer;
use WP_Instances\Worker\WP_REST_API;
use WP_Instances\Worker\Composer_Settings_Admin_Menu;
use WP_Instances\Worker\WP_Core_Settings;

// Bail if accessed directly.
if (!defined('ABSPATH')) {
    die();
}

// Load Composer autoloader.
function composer_autoloader()
{
    $check_for_class = 'WP_Instances\Worker\Composer_Settings_Admin_Menu';

    if (!class_exists($check_for_class, false)) {
        $autoloader = __DIR__ . '/vendor/autoload.php';

        if (!function_exists('get_plugin_data')) {
            require_once ABSPATH . 'wp-admin/includes/plugin.php';
        }

        $plugin_data = get_plugin_data(__FILE__);
        $plugin_name = $plugin_data['Name'];

        if (!file_exists($autoloader)) {
            wp_die(__(sprintf('Please run composer install in the %s plugin directory', $plugin_name), 'wp-instances-worker'));
        }

        require_once $autoloader;

        unset($autoloader);
    }
}

composer_autoloader();

(new WP_REST_API());
(new Composer_Settings_Admin_Menu());

$wp_core_settings = WP_Core_Settings::get();
var_dump($wp_core_settings);

$plugin_service = new WP_Instances\Worker\WP_Plugin_Service();
$plugins = $plugin_service->get_all_plugins();
var_dump($plugins);

function wp_instances_worker_activate() {
    flush_rewrite_rules();
}

function wp_instances_worker_deactivate() {
    flush_rewrite_rules();
}

// Register the activation and deactivation hooks.
register_activation_hook(__FILE__, 'wp_instances_worker_activate');
register_deactivation_hook(__FILE__, 'wp_instances_worker_deactivate');

add_action('init', function () {
	// $theme_manager = new WP_Instances\Worker\Theme_Manager();
	// var_dump($theme_manager->get_all_themes());
    // $composer = new Composer();

    // $acf = $composer->is_plugin_composer_installed('advanced-custom-fields/advanced-custom-fields-pro');
    // $akismet = $composer->is_plugin_composer_installed('akismet/akismet.php');
});
